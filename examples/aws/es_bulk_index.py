from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
import boto3
import json
import sys

host = sys.argv[1]
region = sys.argv[2]

service = 'es'
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(credentials.access_key,
                   credentials.secret_key,
                   region, service)

es = Elasticsearch(
    hosts=[{'host': host, 'port': 443}],
    http_auth=awsauth,
    use_ssl=True,
    verify_certs=True,
    connection_class=RequestsHttpConnection
)


def _get_bulk(file_lines):
    for header in file_lines:
        content = next(file_lines).strip("\n")
        true_content = header + content
        yield true_content


with open(sys.argv[3], "r") as fp:
    bulk = fp.readlines()
    es.bulk(body=_get_bulk(iter(bulk)))
