from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
import boto3
import json
import sys

host = sys.argv[1]
region = sys.argv[2]

service = 'es'
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(credentials.access_key,
                   credentials.secret_key,
                   region, service)

es = Elasticsearch(
    hosts=[{'host': host, 'port': 443}],
    http_auth=awsauth,
    use_ssl=True,
    verify_certs=True,
    connection_class=RequestsHttpConnection
)

with open(sys.argv[3], "r") as fp:
    doc = json.load(fp)
    es.index(index=doc.pop("_index"),
             doc_type=doc.pop("_type"),
             id=doc.pop("_id"),
             body=doc.pop("_source"))
