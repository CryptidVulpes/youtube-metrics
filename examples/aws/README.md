# Amazon Elasticsearch Service helpers
## Usage
### Setup
Before starting, make sure you have proper credentials in the environment by running:

```
aws configure
```

or

```
export AWS_ACCESS_KEY_ID=<access_key>
export AWS_SECRET_ACCESS_KEY=<secret_access_key>
export AWS_DEFAULT_REGION=<region>
```

### Create an index template
Get a template from `youtube_metrics`:

```
youtube_metrics es
```

Push it:

```
python3 es_template.py <AWS_ES_HOST> <AWS_ES_REGION> <PATH_TO_TEMPLATE>
```

### Bulk index `youtube_metrics` analysis
Run analysis:

```
youtube_metrics analyze --output-format bulk-es --output-file analysis.json <video_url> [video_url video_url ...]
```

Run bulk indexer:

```
python3 es_bulk_index.py <AWS_ES_HOST> <AWS_ES_REGION> <PATH_TO_analysis.json>
```

### Deleting an index
```
python3 es_delete_index.py <AWS_ES_HOST> <AWS_ES_REGION> <INDEX_NAME>
```

### Deleting a document
```
python3 es_delete_doc.py <AWS_ES_HOST> <AWS_ES_REGION> <INDEX_NAME> <DOC_ID>
```

### Indexing single documents
Before starting, make sure your document follows this schema:

```json
{
    "_index": <index name>,
    "_type": <document type>,
    "_id": <document id>,
    "_source": {
        <document body>
    }
}
```

These fields are required for the `elasticsearch.Elasticsearch.index()` function call.

Now run:

```
python3 es_index.py <AWS_ES_HOST> <AWS_ES_REGION> <PATH_TO_DOC>
```