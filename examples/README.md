# YouTube Metrics Examples
In `kibana/` there is examples for index pattern, dashboard and visualizations.
Dashboard and visualizations can be imported from `Management > Saved Objects`,
but the index pattern needs to be indexed to ElasticSearch.

In `aws/` there is scripts that can be used to push related data to Amazon
Elasticsearch Service.