# youtube-metrics
Gather network metrics about YouTube videos

Supported Python versions:
* 3.4
* 3.5
* 3.6
* 3.7

## Installation
```
python3 -m pip install youtube-metrics
```

## Usage
```
usage: youtube_metrics [-h] {analyze,es} ...

Gather metrics about a video download.

positional arguments:
  {analyze,es}
    analyze     Analyze video
    es          Print ES template

optional arguments:
  -h, --help    show this help message and exit
```

```
usage: youtube_metrics es [-h]

optional arguments:
  -h, --help  show this help message and exit
```

```
usage: youtube_metrics analyze [-h] [--chunk-size CHUNK_SIZE]
                               [--download-limit DOWNLOAD_LIMIT]
                               [--output-file OUTPUT_FILE]
                               [--output-format OUTPUT_FORMAT]
                               video_url [video_url ...]

positional arguments:
  video_url             The target video to download. Supports same services
                        as youtube_dl.

optional arguments:
  -h, --help            show this help message and exit
  --chunk-size CHUNK_SIZE, -c CHUNK_SIZE
                        Bytesize of each downloaded chunks (each chunk is
                        timed). Default: 1024
  --download-limit DOWNLOAD_LIMIT, -l DOWNLOAD_LIMIT
                        Bytesize of the maximum download before stopping
                        measurements. Default: 41943040
  --output-file OUTPUT_FILE, -of OUTPUT_FILE
                        Filepath for output. Output will be appended to the
                        file. If not defined, stdout is used
  --output-format OUTPUT_FORMAT, -o OUTPUT_FORMAT
                        Format for output. Valid values: json, csv, bulk-es.
                        Default: json
```
### Results
* Time: milliseconds
* Distances: kilometers
* Data: bytes
* Throughput: bytes per second

Results are printed to `OUTPUT_FILE` (Default: `stdout`). Logging is printed to `stderr`

### Example
To pretty-print results, you can use `jq` or `python -m json.tool`:

```json
$ youtube_metrics analyze -c 1 -l 1 "https://www.youtube.com/watch?v=dQw4w9WgXcQ" 2>/dev/null | jq
{
  "me": {
    "ip": "0.0.0.0",
    "coords": {
      "lat": 7.2,
      "long": 5.6
    }
  },
  "videos": [
    {
      "metadata": {
        "id": "dQw4w9WgXcQ",
        "uploader": "RickAstleyVEVO",
        "title": "Rick Astley - Never Gonna Give You Up (Video)",
        "duration": 212000,
        "webpage_url": "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
        "view_count": 487059212,
        "url": "https://r4---sn-qo5-9a4e.googlevideo.com/[...]",
        "ext": "webm",
        "height": 1080,
        "format_note": "1080p",
        "filesize": 48318078,
        "width": 1920,
        "error": ""
      },
      "statistics": {
        "cache_url": "r4---sn-qo5-9a4e.googlevideo.com",
        "ping": {
              "min": 1.505,
              "avg": 1.981,
              "max": 1.679,
              "mdev": 0.503,
              "ip": "62.115.64.87"
        },
        "processing_time": {
          "dns_a_time": 1.0,
          "ping_time": 1.0,
          "dns_aaaa_time": 1.0,
          "ping6_time": 1.0
        },
        "distance": {
            "ip": "62.115.64.87",
            "coordinates": {
              "lat": 42.43,
              "long": -72.64
            },
            "distance": 1
        },
        "download": {
          "downloaded": 2048,
          "throughput": 61394.828157497876,
          "processing_time": {
            "request_time": 0.0,
            "response_time": 0.0
          },
          "format": "webm_1080p"
        }
      }
    }
  ]
}
```