import unittest

from youtube_metrics.lib.net import dns


class DNSTestCase(unittest.TestCase):
    def test_a_query(self):
        self.assertListEqual(sorted(dns.a_query("one.one.one.one")),
                             sorted(["1.1.1.1", "1.0.0.1"]))

    def test_aaaa_query(self):
        self.assertListEqual(sorted(dns.aaaa_query("one.one.one.one")),
                             sorted(["2606:4700:4700::1111",
                                     "2606:4700:4700::1001"]))
