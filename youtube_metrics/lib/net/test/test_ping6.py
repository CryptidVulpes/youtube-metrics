import unittest

from youtube_metrics.lib.net import ping6


class Ping6TestCase(unittest.TestCase):
    def test_ping6_keys(self):
        result = ping6.run("::1")
        self.assertListEqual(sorted(list(result.keys())),
                             sorted(["avg", "max", "min", "mdev"]))

    def test_ping6_items(self):
        result = ping6.run("::1")
        assert_result = [isinstance(_, float) for _ in result.values()]
        self.assertTrue(all(assert_result),
                        msg="At least one value of {} was not of type float"
                        .format(result))
