import unittest

from youtube_metrics.lib.net import quality_measurements


class QMTestCase(unittest.TestCase):
    def test_basic(self):
        results = quality_measurements.basic("one.one.one.one")
        self.assertListEqual(sorted(results.keys()),
                             sorted(["ping",
                                     "processing_time"]))

    def test_advanced(self):
        results = quality_measurements.advanced("one.one.one.one")
        self.assertListEqual(sorted(results.keys()),
                             sorted(["ping",
                                     "processing_time",
                                     "distance"]))
