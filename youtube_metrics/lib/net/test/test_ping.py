import unittest

from youtube_metrics.lib.net import ping


class PingTestCase(unittest.TestCase):
    def test_ping_keys(self):
        result = ping.run("127.0.0.1")
        self.assertListEqual(sorted(list(result.keys())),
                             sorted(["avg", "max", "min", "mdev"]))

    def test_ping_items(self):
        result = ping.run("127.0.0.1")
        assert_result = [isinstance(_, float) for _ in result.values()]
        self.assertTrue(all(assert_result),
                        msg="At least one value of {} was not of type float"
                        .format(result))
