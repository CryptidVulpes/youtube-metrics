# flake8: noqa
import unittest
from unittest.mock import Mock

from youtube_metrics.lib.net.youtube_dl.get_video_info import YTVideoInfo
from youtube_dl.utils import ExtractorError


class YTVideoInfoTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.url = "https://www.youtube.com/watch?v=MxyKczQZ-hM"
        cls.yvi = YTVideoInfo()

    def setUp(self):
        self.mockytdl = Mock()
        self.mockytdl.return_value = self.yvi._ytdl.extract_info(self.url)
        self.yvi._ytdl.extract_info = self.mockytdl

    def tearDown(self):
        self.mockytdl.reset_mock()

    def _yvi_assert(self, exception=None):
        results = self.yvi.get_video_info(self.url)
        self.mockytdl.assert_called_once_with(self.url, download=False)
        if exception is None:
            self.assertIsNone(results["error"])
            self.assertListEqual(sorted(results.keys()),
                                 sorted(["id", "uploader", "title", "duration",
                                         "view_count", "webpage_url", "url",
                                         "ext", "height", "format_note",
                                         "filesize", "width", "error"]))
            self.assertEqual(results["webpage_url"], self.url)
        else:
            self.assertIs(results["error"], exception)

    def test_get_video_info(self):
        self._yvi_assert()

    def test_get_video_info_extractorerror(self):
        error = ExtractorError("Mock Error")
        self.mockytdl.side_effect = error
        self._yvi_assert(exception=error)