# flake8: noqa
import unittest

from youtube_metrics.lib.convert import _flatten_dict


INPUT = {
    "me": {"ip": "my_ip", "coords": {"lat": "my_latitude", "lon": "my_longitude"}},
    "video": {"metadata": {"title": "foo"}}
}


EXPECTED = {
    "me.ip": "my_ip",
    "me.coords.lat": "my_latitude",
    "me.coords.lon": "my_longitude",
    "video.metadata.title": "foo"
}

class ConvertTestCase(unittest.TestCase):
    def test_flatten(self):
        res = _flatten_dict("", INPUT)
        self.assertDictEqual(res, EXPECTED)