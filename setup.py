"""Setup Script."""
from os import path
from sys import exit as _exit

from setuptools import find_packages, setup
from setuptools.command.test import test as TestCommand

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'requirements.txt')) as f:
    requirements = f.read().split("\n")

with open(path.join(here, 'test_requirements.txt')) as f:
    test_requirements = f.read().split("\n")


class PyTest(TestCommand):
    """Custom pytest-runner."""

    user_options = [("pytest-args=", "a", "Arguments to pass to pytest")]

    def initialize_options(self):  # noqa: D102
        TestCommand.initialize_options(self)
        self.pytest_args = ""

    def run_tests(self):  # noqa: D102
        import shlex
        import pytest

        errno = pytest.main(shlex.split(self.pytest_args))
        _exit(errno)


setup(
    name='youtube_metrics',
    version_format='{tag}.post1.dev{commitcount}',
    packages=find_packages(exclude=['test']),
    include_package_data=True,
    package_data={
        '': ['*.txt', '*.mmdb', '*.json']
    },
    setup_requires=['setuptools-git-version'],
    install_requires=requirements,
    tests_require=test_requirements,
    cmdclass={
        "pytest": PyTest
    },
    entry_points={
        'console_scripts': [
            'youtube_metrics=youtube_metrics:main',
        ],
    },
)
